Smusma is going to be a comparison shopping service for *legally-purchased* digital movies and TV shows.
From stores like iTunes and [Vudu](https://www.vudu.com) to code sites.
The idea is that:

- I want to go into the app and see the **best** prices for movies on my wishlist or on sale.
- I don't want to see listings for things I have already purchased or blacklisted.
- If there is a reduction for something on my list that meets my (optional) price target, I want to receive a notification on my phone.
- And if something is not on [MoviesAnywhere](https://moviesanywhere.com/) and my fallback is *only* iTunes, then I don't want to see InstaWatch listings.

On the technical side of things:

- The language of choice is [Golang](https://golang.org).
- The service side of things is going to reside in the cloud.
As such, much architecture inspiration comes from [*Cloud Native Patterns* by Cornelia Davis](https://www.manning.com/books/cloud-native-patterns).
- The cloud provider will likely be [GCP](https://cloud.google.com), as I wasted my free year of [AWS](http://aws.amazon.com) by signing up just before I went on disability. D'oh!
- I want to do things right rather than just bang out an [MVP](https://en.wikipedia.org/wiki/Minimum_viable_product) because this is also a *learning* project for me. That means:
    - [Kubernetes](https://kubernetes.io) with [Docker](https://www.docker.com) containers where the platform and application can be updated separately.
    - Gateways in the pods on both the client and server side.
    - An RDBMS, a NoSQL store, and a K/V cache; using what is available on the cloud platform.
    - Unit testing (of course) as well as CI/CD involving [Jenkins](https://jenkins.io).
- Isn't that a lot for starting out with a little personal project? Perhaps.
It's not that I *need* all of that. It's that I *want to learn* all of that.
- All this will be served onto iOS devices (both iPhone and iPad), not a website (at least at first).
*If* it proves popular, only then will I consider Android devices.